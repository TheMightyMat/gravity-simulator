import math

class Mass:
    def __init__(self, mass, x, y):
        self.__mass = mass
        self.__x = x
        self.__y = y
        self.__velocityX = 0
        self.__velocityY = 0
        self.__isDestroyed = False

    def addMass(self, massToAdd):
        self.__mass += massToAdd

    def getMass(self):
        return self.__mass

    def getRadius(self):
        return math.sqrt(self.__mass / math.pi)

    def getX(self):
        return int(self.__x)

    def getY(self):
        return int(self.__y)

    def getPos(self):
        return self.getX(), self.getY()

    def getVelocity(self):
        return self.__velocityX, self.__velocityY

    def getVelocityMagnitude(self):
        return math.sqrt(math.pow(self.__velocityX, 2) + math.pow(self.__velocityY, 2))

    def updateVelocity(self, delta, force):
        self.__velocityX += (force[0] * delta) / self.__mass
        self.__velocityY += (force[1] * delta) / self.__mass

    def collide(self, otherMass, otherVelocity): # Conserve momentum
        self.__velocityX = (self.__velocityX * self.__mass + otherVelocity[0] * otherMass) / (self.__mass + otherMass)
        self.__velocityY = (self.__velocityY * self.__mass + otherVelocity[1] * otherMass) / (self.__mass + otherMass)

    def move(self, delta):
        self.__x += delta * self.__velocityX
        self.__y += delta * self.__velocityY

    def setPos(self, pos):
        self.__x = pos[0]
        self.__y = pos[1]

    def destroy(self):
        self.__isDestroyed = True

    def isDestroyed(self):
        return self.__isDestroyed
