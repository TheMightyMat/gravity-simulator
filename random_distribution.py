import gravity, random
from mass import Mass

MAX_START_MASS = 10
START_PARTICLES = 200

GRID_DIVISION = 10

size = width, height = 1280, 960

masses = []

size = width, height = gravity.setup(size, fullscreen=False)

for i in range(0, START_PARTICLES):
    massValue = random.randint(1, MAX_START_MASS)
    pos = random.randint(0, width), random.randint(0, height)
    mass = Mass(massValue, pos[0], pos[1])
    masses.append(mass)

gravity.createGrid(GRID_DIVISION)
gravity.addMasses(masses)
gravity.start()
