import gravity, math, random
from mass import Mass

MAX_START_MASS = 2
START_PARTICLES = 1000

GRID_DIVISION = 10

size = width, height = 1280, 960

size = width, height = gravity.setup(size, fullscreen=False)

masses = []

area_per_particle = width * height / START_PARTICLES
step = int(math.sqrt(area_per_particle))
for x in range(step, width, step):
    for y in range(step, height, step):
        massValue = random.randint(1, MAX_START_MASS + 1)
        pos = x, y
        mass = Mass(massValue, pos[0], pos[1])
        masses.append(mass)

gravity.createGrid(GRID_DIVISION)
gravity.addMasses(masses)
gravity.start(timeRate=5)
