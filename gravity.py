import pygame, sys, os, random, time
from mass import *
from gridCell import *

MAX_START_MASS = 2
START_PARTICLES = 200

# GRAV_CONSTANT = 6.67408 * math.pow(10, -11)
GRAV_CONSTANT = 1000 # 1.5x10^13 times gravity, or 15 trillion times gravity, 1 pixel = 1 meter

TIME_RATE = 1

GRID_DIVISION = 10

DEBUG = False

size = width, height = 1280, 960

screen = None

BLACK = (  0,   0,   0)
WHITE = (255, 255, 255)
GRAY  = (100, 100, 100)

grid = []
gridSize = 10;

def updateDisplay(grid):
    global mass
    screen.fill(BLACK)

    font = pygame.font.SysFont("monospace", 15)

    if DEBUG:
        for i in range(0, width, gridSize):
            pygame.draw.line(screen, GRAY, (i, 0), (i, height))

        for i in range(0, height, gridSize):
            pygame.draw.line(screen, GRAY, (0, i), (width, i))

        for cell in grid:
            label = font.render(str(cell.getTotalMass()), 1, GRAY)
            screen.blit(label, (cell.getStartPos()[0] + gridSize/2, cell.getStartPos()[1] + gridSize/2))

    for cell in grid:
        for mass in cell.getMasses():
            if not mass.isDestroyed():
                pygame.draw.circle(screen, WHITE, mass.getPos(), int(mass.getRadius()))


def setup(screenSize, fullscreen=False):
    global gridSize, screen, size, width, height
    pygame.init()

    pygame.mouse.set_visible(False)

    displayInfo = pygame.display.Info()
    displayWidth, displayHeight = displayInfo.current_w, displayInfo.current_h

    width, height = screenSize[0], screenSize[1]

    os.environ['SDL_VIDEO_WINDOW_POS'] = "%d,%d" % (displayWidth / 2 - width / 2, displayHeight / 2 - height / 2)

    if fullscreen:
        size = width, height = displayWidth, displayHeight
        screen = pygame.display.set_mode(size, pygame.FULLSCREEN)
    else:
        size = screenSize
        screen = pygame.display.set_mode(size)

    return size


def createGrid(gridDivision):
    global grid

    gridSize = int(width / gridDivision)

    for x in range(-gridSize, width + gridSize, gridSize):
        for y in range(-gridSize, height + gridSize, gridSize):
            grid.append(GridCell((x, y), (x + gridSize, y + gridSize)))


def addMasses(masses):
    global grid

    for mass in masses:
        addMass(mass)


def addMass(mass):
    global grid

    for cell in grid:
        if cell.massIsWithinBounds(mass):
            cell.addMass(mass)


def start(timeRate=1):
    global size, width, height, screen, gridSize, TIME_RATE, DEBUG

    TIME_RATE = timeRate

    gridSize = int(width / gridSize)

    lastTime = time.time()
    while True:
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                pygame.quit()
                sys.exit()
            if event.type == pygame.KEYDOWN:
                if event.key == pygame.K_F10:
                    DEBUG = False if DEBUG else True
                if event.key == pygame.K_ESCAPE or event.key == pygame.K_F4:
                    pygame.quit()
                    sys.exit()

        updateDisplay(grid)
        delta = (time.time() - lastTime) * TIME_RATE

        __performLogicCalculations(delta, grid, gridSize, height, width)

        pygame.display.update()
        lastTime = time.time()


def __performLogicCalculations(delta, grid, gridSize, height, width):
    for cell in grid:
        cellMasses = cell.getMasses()
        for mass in cellMasses:
            if mass.isDestroyed():
                continue
            netForceX = 0
            netForceY = 0

            for otherMass in cellMasses:  # Masses in same cell
                if (mass is otherMass) or (otherMass.isDestroyed()):
                    continue
                x1, y1 = mass.getPos()
                x2, y2 = otherMass.getPos()

                if __isOutOfBounds(x1, y1, width, height, mass.getRadius()):
                    mass.destroy()
                    continue

                r = math.sqrt(math.pow(x2 - x1, 2) + math.pow(y2 - y1, 2))
                angle = math.atan2(y2 - y1, x2 - x1)

                if r < mass.getRadius() + otherMass.getRadius():
                    __collideMasses(mass, otherMass, x1, x2, y1, y2)
                    continue

                netForceX, netForceY = __addForceToTotal(netForceX, netForceY, GRAV_CONSTANT, mass.getMass(), otherMass.getMass(), r, angle)

            for otherCell in grid:  # Average mass of other cells
                if (cell is otherCell) or (otherCell.getTotalMass() == 0):
                    continue

                netForceX, netForceY = __applyCellForceToMass(gridSize, mass, netForceX, netForceY, otherCell)

            mass.updateVelocity(delta, (netForceX, netForceY))

        moveMasses(cell, cellMasses, delta, grid)


def __applyCellForceToMass(gridSize, mass, netForceX, netForceY, cell):
    x1, y1 = mass.getPos()
    x2 = cell.getStartPos()[0] + (gridSize / 2)
    y2 = cell.getStartPos()[1] + (gridSize / 2)
    r = math.sqrt(math.pow(x2 - x1, 2) + math.pow(y2 - y1, 2))
    angle = math.atan2(y2 - y1, x2 - x1)
    netForceX, netForceY = __addForceToTotal(netForceX, netForceY, GRAV_CONSTANT, mass.getMass(),
                                             cell.getTotalMass(), r, angle)
    return netForceX, netForceY


def moveMasses(cell, cellMasses, delta, grid):
    for mass in cellMasses:
        if mass.isDestroyed():
            continue

        mass.move(delta)
        if not cell.massIsWithinBounds(mass):
            cell.removeMass(mass)

            isOutOfBounds = True
            for tryCell in grid:
                if tryCell.massIsWithinBounds(mass):
                    tryCell.addMass(mass)
                    isOutOfBounds = False

            if isOutOfBounds:
                mass.destroy()


def __collideMasses(m1, m2, x1, x2, y1, y2):
    m1.addMass(m2.getMass())
    m1.collide(m2.getMass(), m2.getVelocity())
    totalMass = m1.getMass() + m2.getMass()
    proportion1, proportion2 = m1.getMass() / totalMass, m2.getMass() / totalMass
    weightedMeanX = (proportion1 * x1 + proportion2 * x2) / (proportion1 + proportion2)
    weightedMeanY = (proportion1 * y1 + proportion2 * y2) / (proportion1 + proportion2)
    m1.setPos((weightedMeanX, weightedMeanY))
    m2.destroy()


def __isOutOfBounds(x, y, width, height, radius):
    return -radius > x > width+radius or -radius > y > height+radius


def __addForceToTotal(netForceX, netForceY, gravConstant, mass1, mass2, distance, angle):
    force = (gravConstant * mass1 * mass2) / math.pow(distance, 2)
    netForceX += force * math.cos(angle)
    netForceY += force * math.sin(angle)
    return netForceX, netForceY


if __name__ == "__main__":
    masses = []

    # Uniform distribution
    area_per_particle = width * height / START_PARTICLES
    step = int(math.sqrt(area_per_particle))
    for x in range(step, width, step):
        for y in range(step, height, step):
            massValue = random.randint(1, MAX_START_MASS + 1)
            pos = x, y
            mass = Mass(massValue, pos[0], pos[1])
            masses.append(mass)

    setup(size)
    createGrid(GRID_DIVISION)
    addMasses(masses)
    start()
