import gravity, random, time, threading
from mass import Mass

MAX_START_MASS = 10
MAX_NEW_MASS = 3
START_PARTICLES = 200

MAX_SPAWN_DELAY = 5

GRID_DIVISION = 10

size = width, height = 1280, 960

def addRandomMasses():
    while True:
        time.sleep(random.uniform(0, MAX_SPAWN_DELAY))

        massValue = random.randint(1, MAX_NEW_MASS)
        pos = random.randint(0, width), random.randint(0, height)
        mass = Mass(massValue, pos[0], pos[1])

        gravity.addMass(mass)


masses = []

size = width, height = gravity.setup(size, fullscreen=True)

for i in range(0, START_PARTICLES):
    massValue = random.randint(1, MAX_START_MASS)
    pos = random.randint(0, width), random.randint(0, height)
    mass = Mass(massValue, pos[0], pos[1])
    masses.append(mass)

gravity.createGrid(GRID_DIVISION)
gravity.addMasses(masses)

t = threading.Thread(target=addRandomMasses)
t.start()

gravity.start()