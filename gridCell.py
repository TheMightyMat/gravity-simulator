class GridCell:
    def __init__(self, startPos, endPos):
        self.__masses = []
        self.__startPos = startPos
        self.__endPos = endPos
        self.__totalMass = 0

    def addMass(self, mass):
        self.__masses.append(mass)
        self.__updateTotalMass()

    def removeMass(self, massToMove):
        index = 0
        while index < len(self.__masses):
            if self.__masses[index] is massToMove:
                self.__masses.pop(index)
                break
            index += 1
        self.__updateTotalMass()

    def getMasses(self):
        return self.__masses

    def getStartPos(self):
        return self.__startPos

    def getEndPos(self):
        return self.__endPos

    def massIsWithinBounds(self, mass):
        x, y = mass.getPos()
        x1, y1 = self.getStartPos()
        x2, y2 = self.getEndPos()

        if x1 <= x <= x2 and y1 <= y <= y2:
            return True
        return False

    def getTotalMass(self):
        return self.__totalMass

    def __updateTotalMass(self):
        self.__totalMass = 0
        for mass in self.__masses:
            if not mass.isDestroyed():
                self.__totalMass += mass.getMass()
